package network.marble.bitmap.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.MapInitializeEvent;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;
import network.marble.bitmap.BitMapRenderer;

public class BitMapListener implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onMapInit(MapInitializeEvent event){
		MapView mapView = event.getMap();
		MapRenderer bmr = new BitMapRenderer(mapView.getId());
		mapView.addRenderer(bmr);
		
		for(MapRenderer mr : mapView.getRenderers()){
			if(!mr.equals(bmr)){
				mapView.removeRenderer(mr);
			}
		}
	}
}
