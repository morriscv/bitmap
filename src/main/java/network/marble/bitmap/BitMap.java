package network.marble.bitmap;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.URL;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.imageio.ImageIO;

import org.bukkit.Bukkit;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import network.marble.bitmap.listeners.BitMapListener;
import network.marble.bitmap.tasks.ImageCollector;
import network.marble.datastoragemanager.database.DAOManager;
import network.marble.datastoragemanager.database.models.MapData;

public class BitMap extends JavaPlugin {
    private static BitMap plugin;
    private static BitMapConfig config;
    
    public static Map<Integer, Image> maps = new HashMap<>();
    
    @Override
    public void onEnable() {
    	if(isDataManagerPresent()){
	        plugin = this;
	        initConfig(false);
	        registerEvents();
	        activateSchedule();
	        getLogger().info("BitMap successfully loaded.");
	        
    	}else{
    		getLogger().severe("Data Manager is not present. Disabling.");
    		setEnabled(false);
    	}
    }
    
    
	@SuppressWarnings("deprecation")
	public static void injectRenderers() {
		for(Entry<Integer, Image> entry: maps.entrySet()){
			MapView mapView = Bukkit.getMap((short)entry.getKey().intValue());
			if(mapView != null){
				MapRenderer bmr = new BitMapRenderer(mapView.getId());
				mapView.addRenderer(bmr);
				
				for(MapRenderer mr : mapView.getRenderers()){
					if(!mr.equals(bmr)){
						mapView.removeRenderer(mr);
					}
				}
			}
		}
		
	}

	private void initConfig(boolean repeat) {
    	try(Reader reader = new FileReader(this.getDataFolder() + "/config.json")){
			config = new Gson().fromJson(reader, BitMapConfig.class);
    		return;
		} catch (Exception e){
			getLogger().info("Generating config.");
		}
    	BitMapConfig builder = new BitMapConfig(); 
    	File directory = this.getDataFolder();
		
    	if (directory.exists() == false){
			try{
				directory.mkdir();
			}catch(Exception e){
				if(repeat){
					this.setEnabled(false);
				}
			}
		}
		
		try(Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.getDataFolder() + "/config.json")))){
			Gson file = new GsonBuilder().create();
			file.toJson(builder, writer);
			getLogger().info("Config file has been created");
		} catch (Exception e){
			e.printStackTrace();
			if(repeat){
				this.setEnabled(false);
			}
		}
		if(!repeat){
			initConfig(true);
		}
	}
    
	public static int[] createImage(String url){
        List<String> formats = Arrays.asList("png", "jpg", "jpeg", "bmp"); 
        
        try {
        	String[] parts = url.split(".");
        	String format = parts[parts.length - 1];
        	int[] maps = null;
        	if(formats.contains(format)){
	        	BufferedImage originalImage = ImageIO.read(new URL(url));
	        	int mapWidth = (int) Math.ceil((double)originalImage.getWidth() / 128d);
	        	int mapHeight = (int) Math.ceil((double)originalImage.getHeight() / 128d);
	        	
	        	maps = new int[mapWidth*mapHeight];
	        	if(originalImage.getWidth() % 128 != 0 || originalImage.getHeight() % 128 != 0){
	        		originalImage = border128(originalImage);
	        	}
	        	
	        	for(int i = 0; i < maps.length; ++i){//position in 1d line of image sections
	        		BufferedImage newImage = new BufferedImage(128, 128, BufferedImage.TYPE_INT_ARGB);
	        		
	        		int startPointY = ((int) Math.floor((double)i / (double)mapWidth)) * 128;//works out y position from specified map width and 1d segment value
	        		for(int j = startPointY; j < startPointY + 128; ++j){
	        			for(int k = (i % mapWidth) * 128; k < i % 128 + 128;++k){
	        				newImage.setRGB(k % 128, j % 128, originalImage.getRGB(k, j));
	        			}
	        		}
	        		
		        	MapData newMap = new MapData();
		        	
		        	ByteArrayOutputStream baos = new ByteArrayOutputStream();
		        	ImageIO.write(newImage, format, baos);
		        	baos.flush();
		        	byte[] byteImage = baos.toByteArray();
		        	baos.close();
					newMap.setData(new String(Base64.getEncoder().encode(byteImage)));
					DAOManager.getMapDataDAO().save(newMap);
	        	}
	        	return maps;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private static BufferedImage border128(BufferedImage originalImage){
		int wOffset = originalImage.getWidth() % 128;
		int hOffset = originalImage.getHeight() % 128;
		Bukkit.getLogger().warning("Width or height of inputted item is not a multiple of 128! Now padding...");
		BufferedImage newImage = new BufferedImage(originalImage.getWidth() + wOffset, originalImage.getHeight() + hOffset, BufferedImage.TYPE_INT_ARGB);
		Graphics g = newImage.getGraphics();
		g.setColor(Color.BLACK);
		if(wOffset>0){
			int leftWLimit = wOffset / 2;
			int remainingW = wOffset - leftWLimit;
			g.drawRect(0, 0, leftWLimit, newImage.getHeight());
			g.drawRect(originalImage.getWidth() + leftWLimit, 0, remainingW, newImage.getHeight());
		}
		
		if(hOffset>0){
			int topHLimit = hOffset / 2;
			int remainingH = hOffset - topHLimit;
			g.drawRect(0, 0, newImage.getWidth(), topHLimit);
			g.drawRect(0, originalImage.getHeight() + topHLimit, remainingH, newImage.getHeight());
		}
		return newImage;
	}
	
    public static void registerEvents(){
    	plugin.getServer().getPluginManager().registerEvents(new BitMapListener(), plugin);
    }
    
    private void activateSchedule(){
		BukkitScheduler scheduler = getServer().getScheduler();
		scheduler.scheduleSyncRepeatingTask(this, new ImageCollector(), 0L, 20 * config.checkTime);
	}
    
    public static BitMap getPlugin() {
        return plugin;
    }
    
    public static BitMapConfig getConfigModel() {
        return config;
    }

    public static boolean isDataManagerPresent(){ 
        return Bukkit.getPluginManager().getPlugin("MRN-DataStorageManager") != null;
    } //Return true if the DataStorageManager is found, and therefore it is safe to use functionality from it, or false otherwise.
}
