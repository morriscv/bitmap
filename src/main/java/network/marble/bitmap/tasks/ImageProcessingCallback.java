package network.marble.bitmap.tasks;

import java.awt.Image;
import java.util.Map;

public interface ImageProcessingCallback {
	
	public void onPlayerImagesProcessed(Map<Integer, Image> images);
	
}
