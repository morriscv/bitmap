package network.marble.bitmap.tasks;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;

import org.bukkit.Bukkit;
import org.mongodb.morphia.query.QueryResults;

import network.marble.bitmap.BitMap;
import network.marble.datastoragemanager.database.DAOManager;
import network.marble.datastoragemanager.database.models.MapData;

public class ImageCollector implements Runnable{
	
	@Override
	public void run() {
		getImages(new ImageProcessingCallback(){
			@Override
			public void onPlayerImagesProcessed(Map<Integer, Image> images){
				if (!images.isEmpty()){
					int i = 0;
					for(Entry<Integer, Image> entry: images.entrySet()){
						BitMap.maps.put(entry.getKey(), entry.getValue());
						if(i < entry.getKey()){i=entry.getKey();}
					}
					//TODO need to replace checker
					BitMap.injectRenderers();
				}
			}
		});
	}
	
	public static void getImages(ImageProcessingCallback callback){
        Bukkit.getScheduler().runTaskAsynchronously(BitMap.getPlugin(), new Runnable(){
        	@Override
        	public void run(){
        		Map<Integer, Image> images = new HashMap<Integer, Image>();
        		QueryResults<MapData> mapdata = DAOManager.getMapDataDAO().find();
        		for(MapData md: mapdata.asList()){
        			
        			BufferedImage bi;
					try {
						bi = ImageIO.read(new ByteArrayInputStream(Base64.getDecoder().decode(md.getData())));
						images.put(md.getMapNumber(), bi);
					} catch (IOException e){
						e.printStackTrace();
					}
        		}
        		
                Bukkit.getScheduler().runTask(BitMap.getPlugin(), new Runnable(){
                    @Override
                    public void run(){
                        callback.onPlayerImagesProcessed(images);
                    }
                });
            }
        });
    }

}
