package network.marble.bitmap;

import java.awt.Image;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapCursor;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;

public class BitMapRenderer extends MapRenderer{
	private int id;
	long lastRefresh = 0L;
	
	public BitMapRenderer(int id){
		this.id = id;
	}
	
	@Override
	public void render(MapView map, MapCanvas canvas, Player player){
		if(lastRefresh + BitMap.getConfigModel().refreshTime*1000 < System.currentTimeMillis()){
			lastRefresh = System.currentTimeMillis();
			Image image = BitMap.maps.get(id);
			if(image != null){
				canvas.drawImage(0, 0, image);
			}else{
				Bukkit.getLogger().info(BitMap.maps.size() + " maps are loaded");
			}
			
			for(int i = 0; i < canvas.getCursors().size(); i++){
				MapCursor curse = canvas.getCursors().getCursor(i);
				canvas.getCursors().removeCursor(curse);
			}
		}
		
	}
}
